import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { TablesComponent } from './component/tables/tables.component';
import { TablesService } from './component/tables/tables.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  declarations: [
    AppComponent,
    TablesComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    JsonpModule,
    NgbModule.forRoot()
  ],
  providers: [TablesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
