import { Component, OnInit } from '@angular/core';
import { TablesService } from './tables.service';

@Component({
  selector: 'app-tables',
  templateUrl: './tables.component.html',
  styleUrls: ['./tables.component.css']
})

export class TablesComponent implements OnInit {

  sortList: any;
  groupMonth: any = [];
  sortMonth: any = [];
  constructor(private svcTb: TablesService) { }

  ngOnInit() {
      // init read sales.json
      this.getSales();
      this.getMonth();
  }

  getSales() {
    this.svcTb.readTables()
      .subscribe(sales =>{
        this.sortTotal(sales.json())
      });
  }
  getMonth(){
    this.svcTb.readTables()
    .subscribe(month =>{
      this.monthTotal(month.json());
    });
  }
  monthTotal(month){
    this.groupMonth = month.resourceList.sort(function(a, b) {
      a = new Date(a.date);
      b = new Date(b.date);
      return a < b ? -1 : a > b ? 1 : 0;
  });
    this.groupByMonth(this.groupMonth);
  }
  sortTotal(sales){ // order by total 
    this.sortList = sales.resourceList.sort((a, b) => parseFloat(a.total) - parseFloat(b.total));
    // send table
  }

  groupByMonth(sales){
    // format col date
    for (let i = 0; i < sales.length; i++){
      sales[i].date = sales[i].date.split("-")[0] + "-" + sales[i].date.split("-")[1];
    }
    this.groupMonth = sales;
    // group by and sum 
    for (let i = 0, j = 0; i < this.groupMonth.length; i++,j++){
      let item = this.groupMonth[i];
      if(i < this.groupMonth.length - 1){
          if( this.groupMonth[i].date == this.groupMonth[i+1].date){
              item.total = this.groupMonth[i].total + this.groupMonth[i+1].total;
              ++i;
            }
            this.sortMonth[j] = item;
      }
      //in the last item
      if(i == this.groupMonth.length - 1){
            this.sortMonth[j] = item;
        }
    }
    }
}
